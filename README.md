# expo-config

expo-config is a port of the popular [Node-Config](https://www.npmjs.com/package/config) package to the Expo Framework. This allows you to create multiple config files, and choosing which to use based on things like environment variables. We are able to do this by taking advantage of Expos `Constants` API and the `app.json` file.

## Installation

simply run the command 

```
npm install --save expo-config
```

## Setup

To setup the config files, please follow the guidelines of the [Node-Config](https://www.npmjs.com/package/config) package for folder structure and config file naming convention.

A quick example might look like at the root of your project:

```
project
│   App.js
│   package.json   
│
└───config
│   │   default.json
│   │   development.json
│   │   development-local.json
│   │   production.json
│   │   ...
│   │
│   
└───src
    │   ...
    │ 
```

A sample config file might look something like:

```JSON
{
  "facebookAppID": "XXXXXX",
  "apiUrl": "http://example.com"
}
```

`expo-config` comes with a cli tool to help streamline everything for you. Here is what your scripts in your package.json might look like now:

```JSON
{
  "scripts": {
    "config:dev-local": "NODE_ENV=development NODE_APP_INSTANCE=local expo-config",
    "config:production": "NODE_ENV=production expo-config",
    "dev": "npm run config:dev-local && expo start",
    "build:android": "npm run config:production && expo build:android"
    ... other scripts
  }
}
```

## Usage

Once you have the package setup in your project, to use the config is quite simple. An example of using the config is as follows:

```javascript
import config from 'expo-config'

console.log(config.facebookAppID)
```

## Notes

One thing I've noticed  is that Expo caches the app.json file pretty good. So if you make a change to the config file, or want to stop the expo server and start up with a new config file, it may require a restart of the simulator to get the new config options.

If anyone has suggestions on improvements, please feel free to create an issue or make a merge request!

### Thank You!


